import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationScreen } from "../Screens";
import SignUpScreen from "../Screens/SignUpScreen/SignUpScreen";
import AmigoScreen from "../Screens/Amigo/AmigoScreen";

const  Stack= createNativeStackNavigator();

export default function NavigationStack(){
    return(
        <Stack.Navigator
         screenOptions={{
        headerShown:false,}}
        >
            <Stack.Screen name='NavigationScreen' component={NavigationScreen}/>
            <Stack.Screen name='SignUpScreen' component={SignUpScreen}/>
            <Stack.Screen name='AmigoScreen' component={AmigoScreen}/>
            
        </Stack.Navigator>
    )
}