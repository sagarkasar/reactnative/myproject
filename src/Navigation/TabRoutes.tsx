import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Entypo from "react-native-vector-icons/Entypo";
import Feather from "react-native-vector-icons/Feather";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import HomeStack from "./HomeStack";
import AccountStack from "./AccountStack";
import { CommonStyles } from "../Styles/CommonStyles";
import colors from "../Styles/colors";
import NavigationStack from "./NavigationStack";
import PramotionalStack from "./PramotionalStack";

const Tab = createBottomTabNavigator();

function TabRoutes() {
  return (
   
      <Tab.Navigator
      initialRouteName="HomeStack"
      screenOptions={{
        headerShown:false,
        tabBarActiveTintColor: colors.$whitecolor,
        tabBarInactiveTintColor: colors.$darkyellow,
        tabBarShowLabel:false,
        tabBarStyle:{
          position:'absolute',
          backgroundColor: colors.$blackcolor,
        },
        
      }}
      >
        <Tab.Screen  
          name='HomeStack'
          component={HomeStack}
          options={{
              tabBarIcon: ({ color }) => (
                <Entypo
                  name='home'
                  size={30}
                   color={color}
                  style={{ marginTop: 10}}
                />
              ),  
            }}
        />
        <Tab.Screen 
        name='NavigationScreen'
        component={NavigationStack}
        options={{
              tabBarIcon: ({ color }) => (
                <Feather
                  name='navigation'
                  color={color}
                  size={30}
                  style={{ marginTop: 10 }}
                />
              ),
            }}
        />
         <Tab.Screen 
        name='PramotionalScreen'
        component={PramotionalStack}
        options={{
              tabBarIcon: ({ color }) => (
                <AntDesign
                  name='tags'
                  color={color}
                  size={35}
                  style={{ marginTop: 10 }}
                />
              ),
            }}
        />
        <Tab.Screen 
        name='AccountScreen'
        component={AccountStack}
        options={{
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons
                  name='account'
                  size={35}
                   color={color}
                  style={{ marginTop: 10 }}
                />
              ),
            }}
        />
      </Tab.Navigator>
   
  );
}

export default TabRoutes;