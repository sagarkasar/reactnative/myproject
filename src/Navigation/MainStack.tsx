import React from "react";
import TabRoutes from "./TabRoutes";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { AccountScreen } from "../Screens";
import SplashScreen from "../Screens/SplashScreen/SplashScreen";
import SignUpScreen from "../Screens/SignUpScreen/SignUpScreen";

const Stack = createNativeStackNavigator();

export default function (){
    return(
        <>
        <Stack.Screen 
        name='SplashScreen'
        component={SplashScreen}
        />
        <Stack.Screen 
        name='AccountScreen'
        component={AccountScreen}
        />
        <Stack.Screen 
        name='TabRoutes'
        component={TabRoutes}
        />
        <Stack.Screen 
        name='SignUpScreen' 
        component={SignUpScreen}
        />
        </>
    )
}