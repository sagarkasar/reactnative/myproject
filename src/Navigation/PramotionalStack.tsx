import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { PramotionalScreen, } from "../Screens";
import SignUpScreen from "../Screens/SignUpScreen/SignUpScreen";


const  Stack= createNativeStackNavigator();

export default function PramotionalStack(){
    return(
        <Stack.Navigator
         screenOptions={{
        headerShown:false,}}
        >
            <Stack.Screen name='PramotionalScreen' component={PramotionalScreen}/>
            <Stack.Screen name='SignUpScreen' component={SignUpScreen}/>
          
            
        </Stack.Navigator>
    )
}