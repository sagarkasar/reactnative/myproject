import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import MainStack from "./MainStack";

const  Stack= createNativeStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      {/* <Stack.Navigator initialRouteName="Home" >
        <Stack.Screen options={{title:'Home Page'}} name={NavigationStrings.HOME} component={Home} />
        <Stack.Screen options={{title:'Profile Page'}} name={NavigationStrings.PROFILE} component={Profile} />
        <Stack.Screen options={{title:'Account Page'}} name={NavigationStrings.ACCOUNT} component={Account} />
      </Stack.Navigator> */}
       <Stack.Navigator screenOptions={{headerShown:false}}>
        {MainStack()}
       </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;