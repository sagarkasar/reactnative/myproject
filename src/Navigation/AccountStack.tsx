import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {  AccountScreen, HomeScreen, } from "../Screens";
import TabRoutes from "./TabRoutes";
import SignUpScreen from "../Screens/SignUpScreen/SignUpScreen";

const  Stack= createNativeStackNavigator();

export default function AccountStack(){
    return(
        <Stack.Navigator
         screenOptions={{
        headerShown:false,}}
        >
            <Stack.Screen name='AccountScreen' component={AccountScreen}/>
            <Stack.Screen name='Home' component={HomeScreen}/>
            <Stack.Screen name='Tab' component={TabRoutes}/>
            <Stack.Screen name='SignUpScreen' component={SignUpScreen}/>
            
           
        </Stack.Navigator>
    )
}