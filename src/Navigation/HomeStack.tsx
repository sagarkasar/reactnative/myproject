import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { SafeAreaProvider } from "react-native-safe-area-context";
import { HomeScreen } from "../Screens";
import ResortScreen from "../Screens/ResortScreen/ResortScreen";
import SignUpScreen from "../Screens/SignUpScreen/SignUpScreen";
import ContactUsScreen from "../Screens/ContactUs/ContactUsScreen";
import ConventionalScreen from "../Screens/Convention/ConventionScreen";

const  Stack= createNativeStackNavigator();

export default function HomeStack(){
    return(
        <SafeAreaProvider>
        <Stack.Navigator  screenOptions={{
        headerShown:false,
        }}>
            <Stack.Screen name='HomeScreen' component={HomeScreen}/>
            <Stack.Screen name='ResortScreen' component={ResortScreen}/>
            <Stack.Screen name='SignUpScreen' component={SignUpScreen}/>
            <Stack.Screen name='ContactUs' component={ContactUsScreen}/>
            <Stack.Screen name='ConventionalScreen' component={ConventionalScreen}/>

        </Stack.Navigator>
        </SafeAreaProvider>
    )
}