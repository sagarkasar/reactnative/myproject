import React, { useState } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { CommonStyles, TopImage } from "../Styles/CommonStyles";

const TopIcon = () => {
  return (
    <>
      <View>
        <Image
          source={TopImage}
          resizeMode='center'
          style={CommonStyles.topimage}
        />
      </View>
    </>
  );
};

export default TopIcon;
