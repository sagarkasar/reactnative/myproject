import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
interface DrodownProperty {
  items: Array<{ label: string; value: any }>;
  placeholder: string;
  defaultValue: string;
  //icons: string;
  onSelect?: ((value: string, index: number) => void) | undefined;
}


const DropDown = (props: DrodownProperty ) => {
    return (
        <View>
           <RNPickerSelect
        value={props.defaultValue}
        useNativeAndroidPickerStyle={false}
        onValueChange={props.onSelect}
        style={stylesPicker}
        items={props.items}
        placeholder={{
          label: props.placeholder,
        }}
        />
        </View>
    )
}

export default DropDown

const stylesPicker = StyleSheet.create({
  placeholder: {
    color: "#999999",
    fontSize: 14,
    paddingLeft: 40,
  },
  inputAndroid:{
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderWidth: 1,
    borderColor: 'gray',
    backgroundColor: 'white',
    color: "#000",
    width: 300,
    height: 40,
    fontSize: 14,
    marginBottom: 10,
    padding: 10,
  },
 iconContainer:{
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 16,
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderWidth: 1,
    borderColor: 'gray',
    backgroundColor: 'white',
    color: "#000",
    width: 100,
    marginStart: 20,
    fontSize: 14,
    marginBottom: 10,
    padding: 10,
  },
});
const styles = StyleSheet.create({})
   
