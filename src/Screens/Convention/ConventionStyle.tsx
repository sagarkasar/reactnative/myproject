import { StyleSheet,  } from "react-native";
import colors from "../../Styles/colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
export const ConventionalStyle= StyleSheet.create({
    guestimage4: {
    height: hp(50),
    marginLeft: wp(6),
    width: wp(88),
    marginRight: wp(5),
    backgroundColor: "transparent",
    alignItems: "flex-start",
  },
  maintxt: {
    color: colors.$darkyellow,
    textAlign: 'center',
    fontSize: RFPercentage(4.5),
    fontWeight: '700'
  },
  numberstyle:{
    paddingLeft: wp(1),
    color: colors.$whitecolor,
    fontSize: RFPercentage(2.7),
    fontWeight:'700'
  },
  divider:{
      marginVertical: hp(2),
      marginHorizontal: wp(30),
  },
  hotel:{
      height: hp(35),
      width: wp(100),
      backgroundColor: "transparent",
      alignItems: "flex-start",
  },
  imgtext:{
      paddingLeft: wp(1),
      color: colors.$darkyellow,
      fontSize: RFPercentage(3.5),
      alignSelf:'center'
  },
  numberstyle1:{
    paddingLeft: wp(1),
    color: colors.$whitecolor,
    fontSize: RFPercentage(2.2),
    fontWeight:'500'
  },
})