import React, { useState} from 'react';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import Fontisto from "react-native-vector-icons/Fontisto";
import TopIcon from '../../ReusableComponunts/TopIcon';
import { StyleSheet, View, Button, Text, Image, ScrollView, TextInput, Touchable, TouchableOpacity, ImageBackground, Linking, } from 'react-native';
import {  Divider } from 'react-native-elements';
import * as Animatable from "react-native-animatable";
import { Brid, GuestRoom, GuestRoom1, GuestRoom2, } from '../../Styles/CommonStyles';
import colors from '../../Styles/colors';
import { ConventionalStyle } from './ConventionStyle';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
const ConventionalScreen =  ({navigation}: any) =>{

    const url1 = "https://sites.google.com/theguestlink.com/join/your-website";
    const openUrl = async (url: any) => {
    const isSuported = await Linking.canOpenURL(url);
    if (isSuported) {
      await Linking.openURL(url);
    } 
    };

   return( 
   <View style={{ backgroundColor: colors.$blackcolor, flex: 1 }}>
       <TopIcon/>
    <ScrollView>
        <ImageBackground
            source={Brid}
            resizeMode='contain'
            style={ConventionalStyle.guestimage4}
        />
        <Animatable.Text
            style={ConventionalStyle.maintxt}
            animation='fadeInLeftBig'>
            Cape Canaveral Test Convention Center
        </Animatable.Text>
        <Animatable.View
            style={{flexDirection:'row', marginTop: hp(2)}}
            animation='fadeInRightBig'>
            <FontAwesome
                name='photo'
                size={20}
                color={colors.$whitecolor}
                style={{ paddingLeft: wp(14) }}
              />
              <Text style={ConventionalStyle.numberstyle}>1</Text>
              <Text
                style={ConventionalStyle.numberstyle}>
                Photos
              </Text>

              <Ionicons
                name='ios-location-sharp'
                size={20}
                style={{ paddingLeft: wp(10) }}
                color={colors.$whitecolor}
              />

              <Text
                style={[
                  ConventionalStyle.numberstyle,
                  {
                    color: colors.$darkyellow,
                  },
                ]}>
                333 State St,
                {"\n"}Erie, PA 16507, USA
              </Text>
        </Animatable.View>
        <Divider width={2} color={colors.$darkyellow} style={ConventionalStyle.divider}/>
        <View style={{ flexDirection: "row", justifyContent:'space-between',marginHorizontal: wp(30), marginTop: hp(1) }}>
                <Ionicons
                onPress={() => {
                  openUrl(url1);
                }}
                name='earth'
                size={30}
               
                color={colors.$whitecolor}
              />

              <AntDesign
                name='pluscircle'
                size={30}
                color={colors.$whitecolor}
              />

              <FontAwesome
                name='tag'
                size={30}
                color={colors.$whitecolor}
              />
        </View>
            <MaterialCommunityIcons
              name='account-group'
              size={44}
              style={{ alignSelf: "center", marginTop: hp(5)}}
              color={colors.$darkyellow}
            />
            <Divider width={2} color={colors.$darkyellow} style={[ConventionalStyle.divider,{marginHorizontal: wp(42)}]}/>
            <TouchableOpacity>
                <Animatable.Image
                    animation='fadeInDown'
                    source={GuestRoom}
                    style={ConventionalStyle.hotel}
                    resizeMode='cover'
                />
            </TouchableOpacity>
            <AntDesign
              name='tags'
              size={30}
              style={{ textAlign: "center", marginTop: hp(2) }}
              color={colors.$darkyellow}
            />
            <Divider width={2} color={colors.$darkyellow} style={[ConventionalStyle.divider,{marginHorizontal: wp(42)}]}/>
            <TouchableOpacity>
                <Animatable.Image
                    animation='fadeInDown'
                    source={GuestRoom1}
                    style={ConventionalStyle.hotel}
                    resizeMode='cover'
                />
            </TouchableOpacity>
            <Text style={ConventionalStyle.imgtext}> Awesome Event Test Deal!</Text>
            <View style={{flexDirection:'row'}}>
                <Entypo
                  name='trophy'
                  size={20}
                  style={{ paddingLeft: wp(1.5) }}
                  color={colors.$whitecolor}
                />
                <Text style={ConventionalStyle.numberstyle1}>400.00 Points</Text>

                <Entypo
                  name='trophy'
                  size={20}
                  style={{ paddingLeft: wp(1.5) }}
                  color={colors.$whitecolor}
                />
                <Text style={ConventionalStyle.numberstyle1}>Offer code : vgtxin</Text>

                <Fontisto
                  name='date'
                  size={15}
                  style={{ paddingLeft: wp(2) }}
                  color={colors.$whitecolor}
                />
                <Text style={[ConventionalStyle.numberstyle1, { paddingRight: wp(2)}]}>
                  2021-08-12
                </Text>
            </View>
              <Ionicons
                name='home'
                size={30}
                style={{ textAlign: "center", marginTop: hp(2) }}
                color={colors.$darkyellow}
              />
             <Divider width={2} color={colors.$darkyellow} style={[ConventionalStyle.divider,{marginHorizontal: wp(42)}]}/>
            <TouchableOpacity>
                <Animatable.Image
                    animation='fadeInDown'
                    source={GuestRoom2}
                    style={ConventionalStyle.hotel}
                    resizeMode='cover'
                />
            </TouchableOpacity>
            <Divider width={2} color={colors.$darkyellow} style={[ConventionalStyle.divider,{marginHorizontal: wp(42)}]}/>
            <TouchableOpacity>
                <Animatable.Image
                    animation='fadeInDown'
                    source={GuestRoom2}
                    style={ConventionalStyle.hotel}
                    resizeMode='cover'
                />
            </TouchableOpacity>
              <View style={{ marginBottom: 70}}></View>
    </ScrollView>
    </View>
   )
}

export default ConventionalScreen;