import React, { Component } from 'react';
import { Button, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import colors from '../../Styles/colors';
import TopIcon from '../../ReusableComponunts/TopIcon';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { pramotionalStyle } from './PramotionalStyle';
import { Video, AVPlaybackStatus } from 'expo-av';


const PramotionalScreen = ({navigation}: any) => {
    const GotoAccount = () =>{}
    const video = React.useRef(null);
    const [status, setStatus] = React.useState({});

    return (
        <View style={{ backgroundColor: colors.$blackcolor, flex: 1}}>
        <TopIcon/>
          <View style={pramotionalStyle.container}>
           <Text style={pramotionalStyle.txt}>Currently, no Offers match your search criteria</Text>
          </View>
        </View>
    )
}

export default PramotionalScreen;
