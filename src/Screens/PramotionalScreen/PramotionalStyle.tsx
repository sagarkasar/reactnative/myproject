import { StyleSheet } from "react-native";
import colors from "../../Styles/colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";


export const pramotionalStyle = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: colors.$blackcolor,
    alignItems: 'center',
    marginTop: 20,
    justifyContent:'center'
  },
  txt:{
    fontSize: RFPercentage(4),
    color: colors.$darkyellow,
    fontWeight: '600',
  }
 
});