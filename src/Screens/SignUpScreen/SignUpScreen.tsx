import React, {useState} from 'react'
import { ScrollView, StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { RFPercentage } from "react-native-responsive-fontsize";
import * as yup from "yup";
import { Formik } from "formik";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import colors from '../../Styles/colors'
import TopIcon from '../../ReusableComponunts/TopIcon'
import { CommonStyles } from '../../Styles/CommonStyles';
import NavigationStack from '../../Navigation/NavigationStack';
import { SignUpStyle } from './SignUpStyle';
import axios from 'axios';
import Ionicons from "react-native-vector-icons/Ionicons";
import { accountStyle } from '../AccountScreen/AccountStyle';



const SignUpScreen = ({navigation}: any) => {
    const [secure, setSecure] = React.useState(true);
    const [secure1, setSecure1] = React.useState(true);

    const [FirstName, setFirstName] = useState("");
    const [LastName, setLastName] = useState("");
    const [Email, setEmail] = useState("");
    const [Password, setPassword] = useState("");
    const [ConfirmPassword, setConfirmPassword] = useState("");
    const [Phone, setPhone] = useState("");
    const onSubmit = async (values: any) => {
        navigation.navigate('AccountScreen')
        console.log(values)
        const fname = values.FirstName;
        const lname = values.LastName;
        const email = values.Email;
        const phone = values.Phone;
        const pass = values.Password;
        let bodyFormData = new FormData();
            
            bodyFormData.append("signup_fname", fname );
            bodyFormData.append("signup_lname", lname );
            bodyFormData.append("signup_email", email );
            bodyFormData.append("signup_phone", phone );
            bodyFormData.append("signup_password", pass );
            // signup_fname:Kunal
            // signup_lname:K
            // signup_email:supraprojects@yahoo.com
            // signup_phone:1234567890
            // signup_password:Ktest@123
            return axios({
              method: "post",
              //url: "https://api.sp2systems.net/userlogin",
              url: "https://www.guestgold.com/CheckLoginAuthentication/apiSignUp",
              data: bodyFormData,
              headers: {
                "Content-Type": "multipart/form-data",
                "Access-Control-Allow-Origin": "*",
                
                //"Access-Control-Allow-Headers: Content-Type",
              },
            })
              .then(function (response) {
                //handle success
                console.log(response);
                console.log("Login Details", response.data);
                return "success";
              })
              .catch(function (response) {
                //handle error
                console.log(response);
                return "error";
              });
    };

    return (
        <View style={{ backgroundColor: colors.$blackcolor, flex: 1 }}>
            <TopIcon />
            <ScrollView>
                <Formik
          initialValues={{
            FirstName: "",
            LastName: "",
            Email: "",
            Phone: "",
            Password: "",
            ConfirmPassword: "",
          }}
          onSubmit={onSubmit}
          validationSchema={yup.object().shape({
            FirstName: yup.string().required(),
            LastName: yup.string().required(),
            Email: yup.string().email().required(),
            Phone: yup.string().required(),
            Password: yup.string().required(),
            ConfirmPassword: yup
              .string()
              .equals([yup.ref("Password")], "Password don't match")
              .required("Required"),
          })}>
          {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              handleSubmit,
          }) => (
                <View>
                <Text style={SignUpStyle.signuptxt}>Create an Account</Text>
                <Text style={SignUpStyle.nametext}>First Name</Text>
                <TextInput
                    style={SignUpStyle.inputtxt}
                    placeholder='First Name'
                    placeholderTextColor= {colors.$darkyellow}
                    defaultValue={values.FirstName}
                    onChangeText={handleChange('FirstName')}
                    onChange={() => setFirstName(FirstName)}
                    onBlur={()=> setFieldTouched('FirstName')}
                />
                {touched.FirstName && errors.FirstName && (
                <Text style={[CommonStyles.error,{marginTop: wp(-3) }]}>{errors.FirstName}</Text>
                )}
                <Text style={SignUpStyle.nametext}>Last Name</Text>
                <TextInput
                    style={SignUpStyle.inputtxt}
                    placeholder='Last Name'
                    placeholderTextColor= {colors.$darkyellow}
                    defaultValue={values.LastName}
                    onChangeText={handleChange('LastName')}
                    onChange={() => setLastName(LastName)}
                    onBlur={()=> setFieldTouched('LastName')}
                />
                {touched.LastName && errors.LastName && (
                <Text style={[CommonStyles.error,{marginTop: wp(-3) }]}>{errors.LastName}</Text>
                )}
                <Text style={SignUpStyle.nametext}>Email</Text>
                <TextInput
                    style={SignUpStyle.inputtxt}
                    placeholder='Email'
                    placeholderTextColor= {colors.$darkyellow}
                    defaultValue={values.Email}
                    onChangeText={handleChange('Email')}
                    onChange={() => setEmail(Email)}
                    onBlur={()=> setFieldTouched('Email')}
                />
                {touched.Email && errors.Email && (
                <Text style={[CommonStyles.error,{marginTop: wp(-3) }]}>{errors.Email}</Text>
                )}
                <Text style={SignUpStyle.nametext}>Phone</Text>
                <TextInput
                    style={SignUpStyle.inputtxt}
                    placeholder='Phone'
                    placeholderTextColor= {colors.$darkyellow}
                    defaultValue={values.Phone}
                    onChangeText={handleChange('Phone')}
                    onChange={() => setPhone(Phone)}
                    onBlur={()=> setFieldTouched('Phone')}
                />
                {touched.Phone && errors.Phone && (
                <Text style={[CommonStyles.error,{marginTop: wp(-3) }]}>{errors.Phone}</Text>
                )}
                <Text style={SignUpStyle.nametext}>Password</Text>
                <TextInput
                    secureTextEntry={secure}
                    style={SignUpStyle.inputtxt}
                    placeholder='Password'
                    placeholderTextColor= {colors.$darkyellow}
                    defaultValue={values.Password}
                    onChangeText={handleChange('Password')}
                    onChange={() => setPassword(Password)}
                    onBlur={()=> setFieldTouched('Password')}
                />
                <Ionicons 
                name={secure ? "eye-off" : 'eye'}
                size={25}
                color={colors.$darkyellow}
                style={SignUpStyle.showicon}
                onPress={() => setSecure(!secure)}
                /> 
                {touched.Password && errors.Password && (
                <Text style={[CommonStyles.error,{marginTop: wp(-5) }]}>{errors.Password}</Text>
                )}
                <Text style={SignUpStyle.nametext}>Confirm Password</Text>
                <TextInput
                    secureTextEntry={secure1}
                    style={SignUpStyle.inputtxt}
                    placeholder='Confirm Password'
                    placeholderTextColor= {colors.$darkyellow}
                    defaultValue={values.ConfirmPassword}
                    onChangeText={handleChange('ConfirmPassword')}
                    onChange={() => setConfirmPassword(ConfirmPassword)}
                    onBlur={()=> setFieldTouched('ConfirmPassword')}
                />
                <Ionicons 
                name={secure1 ? "eye-off" : 'eye'}
                size={25}
                color={colors.$darkyellow}
                style={SignUpStyle.showicon}
                onPress={() => setSecure1(!secure1)}
                /> 
                {touched.ConfirmPassword && errors.ConfirmPassword && (
                <Text style={[CommonStyles.error,{marginTop: wp(-5) }]}>{errors.ConfirmPassword}</Text>
                )}
                <TouchableOpacity 
                style={SignUpStyle.signupcontainer} 
                onPress={()=>handleSubmit()}
                disabled={!isValid}
                >
                    <Text style={SignUpStyle.signupbtn}>Signup</Text>
                </TouchableOpacity>
                </View>
                )}
                </Formik>
            </ScrollView>
            
        </View>
    )
}

export default SignUpScreen