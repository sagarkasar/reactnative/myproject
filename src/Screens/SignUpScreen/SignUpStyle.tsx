import { StyleSheet } from "react-native";
import colors from "../../Styles/colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";


export const SignUpStyle = StyleSheet.create({
   signuptxt:{
        color: colors.$darkyellow,
        fontSize: RFPercentage(3.4),
        fontWeight: '700',
        marginLeft: wp(6),
        marginTop: hp(2),
        marginBottom: hp(1.5),
    },
    nametext:{
        color: colors.$darkyellow,
        fontSize: RFPercentage(2.2),
        marginLeft: wp(6),
        marginTop: hp(0.8),
    },
    inputtxt:{
        backgroundColor: colors.$whitecolor,
        padding: '3%',
        height: hp(5.2),
        margin: '3%',
        borderWidth: 1,
        marginLeft: wp(6),
        marginRight: wp(5),
        borderRadius: wp(1)
    },
    signupcontainer:{
        height: hp(5),
        width: wp(30),
        backgroundColor: colors.$darkyellow,
        alignSelf: "flex-start",
        marginTop: hp(2),
        marginStart: wp(7),
        borderRadius: wp(2),
        marginBottom: hp(8)
    },
    signupbtn:{
        color: colors.$whitecolor,
        fontSize: RFPercentage(2.4),
        letterSpacing: 2,
        textAlign: "center",
        marginTop: hp(1),
        fontWeight: '700',
    },
    showicon:{
        alignSelf:'flex-end',
        marginRight: wp(5.3),
        marginTop: wp(-13.4),
        marginBottom: wp(6),
        padding: wp(1.4),
        backgroundColor:colors.$darkgrey,
        borderRadius: 3
    }
});