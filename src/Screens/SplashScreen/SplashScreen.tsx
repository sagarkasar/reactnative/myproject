import React from 'react'
import { Image, StyleSheet, Text, Touchable, TouchableOpacity, View } from 'react-native'
import colors from '../../Styles/colors';
import { CommonStyles, TopImage } from '../../Styles/CommonStyles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { SplashStyle } from './SplashStyle';

const SplashScreen = ({navigation}: any) => {
    return (
         <View style={{ backgroundColor: colors.$blackcolor, flex: 1 }}>
        
            <View style={SplashStyle.container}>
                <Image source={TopImage} style={SplashStyle.iconimg}/>
                <Text style={SplashStyle.textlogo}>Guest Gold</Text>
                <Text style={SplashStyle.text}>Real estate cannot be lost or stolen, nor can it be carried away</Text>
                <TouchableOpacity>
                    <Text style={SplashStyle.btn} onPress={()=> navigation.navigate('AccountScreen')}>Get Started</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default SplashScreen