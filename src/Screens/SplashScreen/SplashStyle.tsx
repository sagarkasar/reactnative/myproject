import { StyleSheet } from "react-native";
import colors from "../../Styles/colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";


export const SplashStyle = StyleSheet.create({
   container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1/2,
      borderColor: colors.$whitecolor,
      height: hp('86%'),
      width: wp('86%'),
      margin: '7%',
      backgroundColor: '#111',
      marginTop: wp(14),
      fontFamily:'Century Gothic',   
  },
  textlogo:{
      color:colors.$darkyellow,
      fontSize: RFPercentage(5),
      fontWeight: '700',
  },
  iconimg:{
      height: hp('16%'),
      width: wp('16%'),
      marginBottom: hp(3)
  },
  text:{
      fontSize: RFPercentage(3),
      color:colors.$whitecolor,
      textAlign:'center',
      marginTop: hp(1),
      marginHorizontal: wp(6)
  },
  btn:{
    backgroundColor: colors.$darkyellow,
    color: colors.$whitecolor,
    padding: '3.2%',
    borderRadius: 40,
    height: hp('6%'),
    width: wp('60%'),
    fontSize: RFPercentage(2.5),
    textAlign: 'center',
    fontWeight: "700",
    marginTop: hp('6%')
  }
});