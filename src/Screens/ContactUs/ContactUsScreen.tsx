import React, { Component, useState } from 'react';
import { Button, ScrollView, StyleSheet, Text, View, TextInput, TouchableOpacity, Linking } from 'react-native';
import colors from '../../Styles/colors';
import TopIcon from '../../ReusableComponunts/TopIcon';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ContactStyle } from './ContactStyle';
import { Formik } from 'formik';
import * as yup from "yup";
import { CommonStyles } from '../../Styles/CommonStyles';
import Ionicons from "react-native-vector-icons/Ionicons";
import Foundation from "react-native-vector-icons/Foundation";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import AntDesign from "react-native-vector-icons/AntDesign";

const ContactUsScreen = ({navigation}: any) => {
        const [name, setname] = useState("");
        const [email, setemail] = useState("");
        const [subject, setsubject] = useState("");
        const [message, setmessage] = useState("");
        const onSubmit = async (values: any) => {
            var nameval = values.name;
        };
        const url1 = "https://m.facebook.com/GuestGoldDotCom";
        const url2 = "https://mobile.twitter.com/GuestGoldDotCom";
        const url3 = "https://www.instagram.com/guestgolddotcom/";
        const openUrl = async (url: any) => {
            const isSuported = await Linking.canOpenURL(url);
            if (isSuported) {
            await Linking.openURL(url);
            } else {
            //  Alert.alert{`Not Found Url: $(url))`};
            }
        };
    return (
        <View style={{ backgroundColor: colors.$blackcolor, flex: 1 }}>
        <TopIcon/>
        <Formik
            initialValues={{ name: "", email: "", subject: "", message: "" }}
            onSubmit={onSubmit}
            validationSchema={yup.object().shape({
              name: yup.string().required(),
              email: yup.string().email().required(),
              subject: yup.string().required(),
              message: yup.string().required(),
            })}>
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              handleSubmit,
            }) => (
        <ScrollView>
            <Text style={ContactStyle.maintxt}>Contact Us</Text>
            <View>
                 <TextInput
                    style={ContactStyle.inputfield}
                    placeholder='Name *'
                    placeholderTextColor='#555'
                    defaultValue={values.name}
                    onChangeText={handleChange("name")}
                    onChange={() => setname(name)}
                    onBlur={() => setFieldTouched("name")}
                  />
                  {touched.name && errors.name && (
                    <Text style={CommonStyles.error}>{errors.name}</Text>
                  )}

                  <TextInput
                    style={ContactStyle.inputfield}
                    placeholder='Email *'
                    placeholderTextColor='#555'
                    defaultValue={values.email}
                    onChangeText={handleChange("email")}
                    onChange={() => setemail(email)}
                    onBlur={() => setFieldTouched("email")}
                  />
                  {touched.email && errors.email && (
                    <Text style={CommonStyles.error}>{errors.email}</Text>
                  )}
                  <TextInput
                    style={ContactStyle.inputfield}
                    placeholder='Subject *'
                    placeholderTextColor='#555'
                    defaultValue={values.subject}
                    onChangeText={handleChange("subject")}
                    onChange={() => setsubject(subject)}
                    onBlur={() => setFieldTouched("subject")}
                  />
                  {touched.subject && errors.subject && (
                    <Text style={CommonStyles.error}>{errors.subject}</Text>
                  )}
                  <TextInput
                    multiline
                    numberOfLines={2}
                    style={[ContactStyle.inputfield, { height: hp(7.8) }]}
                    placeholder='Message *'
                    placeholderTextColor='#555'
                    maxLength={40}
                    defaultValue={values.message}
                    onChangeText={handleChange("message")}
                    onChange={() => setmessage(message)}
                    onBlur={() => setFieldTouched("message")}
                  />
                  {touched.message && errors.message && (
                    <Text style={CommonStyles.error}>{errors.message}</Text>
                  )}
                  <TouchableOpacity
                    style={ContactStyle.scancontainer}
                    onPress={() => handleSubmit()}
                    disabled={!isValid}>
                    <Text style={ContactStyle.scantext}>Send Message</Text>
                  </TouchableOpacity>

                  <View>
                    <Text style={ContactStyle.contacttxt}>CONTACT INFORMATION</Text>

                    <View style={{ flexDirection: "row" }}>
                      <Ionicons
                        name='location-sharp'
                        color='#fff'
                        size={20}
                        style={{ marginStart: wp(4) }}
                      />
                      <Text style={ContactStyle.icontxt}>Location:</Text>
                      <Text style={ContactStyle.locationtxt}>Here</Text>
                    </View>
                    
                    <View style={{ flexDirection: "row", marginTop: hp(2) }}>
                      <Foundation
                        name='telephone'
                        color='#fff'
                        size={20}
                        style={{ marginStart: wp(4) }}
                      />
                      <Text style={ContactStyle.icontxt}>Phone number:</Text>
                      <Text style={ContactStyle.locationtxt}> 716.508.7706 </Text>
                    </View>

                    <View style={{ flexDirection: "row", marginTop: hp(2) }}>
                      <MaterialIcons
                        name='email'
                        color='#fff'
                        size={20}
                        style={{ marginStart: wp(4) }}
                      />
                      <Text style={ContactStyle.icontxt}>E-mail: </Text>
                      <Text style={ContactStyle.emailtxt}>
                        {" "}
                        Contact@GuestGold.com{" "}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      bottom: hp(2),
                      alignSelf: "center",
                      //marginStart: wp(7),
                    }}>
                    <EvilIcons
                      onPress={() => {
                        openUrl(url1);
                        }}
                      name='sc-facebook'
                      size={27}
                      color='#d6af35'
                      style={{ marginTop: hp(4.3), marginStart: wp(5) }}
                    />

                    <AntDesign
                      onPress={() => {
                        openUrl(url2);
                      }}
                      name='twitter'
                      size={20}
                      color='#d6af35'
                      style={{ marginTop: hp(4.3), marginStart: wp(5) }}
                    />

                    <AntDesign
                      onPress={() => {
                        openUrl(url3);
                      }}
                      name='instagram'
                      size={20}
                      color='#d6af35'
                      style={{
                        marginTop: hp(4.3),
                        marginStart: wp(6),
                      }}
                    />
                  </View>
                  <Text style={{marginBottom: 30}}></Text>
                </View>
              </ScrollView>
            )}
          </Formik>
           
        </View>
    )
}

export default ContactUsScreen;