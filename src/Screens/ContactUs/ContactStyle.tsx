import { StyleSheet } from "react-native";
import colors from "../../Styles/colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";


export const ContactStyle = StyleSheet.create({
    maintxt:{
        fontSize: 28,
        color: colors.$darkyellow,
        fontWeight:'700',
        textAlign:'center',
        marginTop: wp(2),
    },
  contacttxt: {
    color: colors.$darkyellow,
    fontSize: RFPercentage(2.5),
    fontWeight:'700',
    textAlign: "center",
    marginTop: hp(4),
    },
    inputfield: {
    borderStyle: "solid",
    paddingLeft: 20,
    paddingRight: 20,
    borderWidth: 1,
    borderColor: "#cccccc",
    height: hp(6),
    width: wp(80),
    marginTop: hp(4),
    backgroundColor: colors.$whitecolor,
    overflow: "hidden",
    color: colors.$blackcolor,
    fontSize: RFPercentage(2.1),
    marginStart: wp(6.5),
  },
  submitbtn: {
    width: 140,
    marginLeft: 15,
    marginTop: 20,
    borderRadius: 15,
  },
  icontxt: {
    color: "#fff",
    fontSize: RFPercentage(2.2),
    marginLeft: wp(3),
    marginTop: hp(-0.2),
  },
  icondiv: {
    marginLeft: 15,
    marginTop: 30,
  },
  locationtxt: {
    color: "#fff",
    marginLeft: wp(5),
    fontSize: RFPercentage(2.2),
  },
  scantext: {
    color: colors.$whitecolor,
    fontSize: RFPercentage(2.2),
    letterSpacing: 2,
    textAlign: "center",
    marginTop: hp(1.4),
  },
  scancontainer: {
    height: hp(6.5),
    width: wp(45),
    marginLeft: wp(-40),
    backgroundColor: colors.$darkyellow,
    alignSelf: "center",
    marginTop: hp(3),
    borderRadius: 5,
  },
  emailtxt: {
    color: colors.$darkyellow,
    marginLeft: wp(3),
    fontSize: RFPercentage(2.2),
  },
});