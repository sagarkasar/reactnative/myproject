import React from 'react'
import { Image, ScrollView, StyleSheet, Text, View, TextInput } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import colors from '../../Styles/colors';
import TopIcon from '../../ReusableComponunts/TopIcon';
import { CommonStyles, GuestImage } from '../../Styles/CommonStyles';
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { Divider } from 'react-native-paper';

const ResortScreen = ({navigation}:any) => {
    return (
        <View style={{ flex: 1 }}>
        <TopIcon/>
            <ScrollView>
            <Text style={styles.text}>Resort Details</Text>
            <Image source={GuestImage} style={styles.image1}/>
            <Text style={styles.text}>Sea Island Resort</Text>
            <View style={styles.iconlocation}> 
                <Ionicons
                  name='location-sharp'
                  size={14}
                  color={colors.$darkyellow}
                />
                 <Text style={styles.icontext}> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
            </View>
            <View style={styles.iconlocation}> 
                <MaterialIcons
                  name='photo'
                  size={14}
                  
                  color={colors.$grey}
                />
                 <Text style={styles.icontext}> 4 Photos</Text>
            </View>
            <View style={styles.iconlocation}> 
                <Entypo
                  name='star'
                  size={14}
                  
                  color={colors.$grey}
                />
                 <Text style={styles.icontext}> 4.5 Rating</Text>
            </View>
            <Divider style={styles.divider}/>
            <View style={styles.iconlocation}> 
            <Text style={styles.offertxt}>Offer</Text>
            </View>
            <View style={styles.iconlocation}> 
                <Ionicons
                  name='trophy'
                  size={14}
                  
                  color={colors.$grey}
                />
                 <Text style={styles.icontext}> 20,00 Points</Text>
            </View>
            <View style={styles.iconlocation}> 
                <MaterialIcons
                  name='local-offer'
                  size={14}
                  
                  color={colors.$grey}
                />
                 <Text style={styles.icontext}> Offer Code <Text style={{fontWeight:'700', color: colors.$blackcolor}}>rdHdtO</Text></Text>
            </View>
            <View style={styles.iconlocation}> 
                <FontAwesome5

                  name='calendar-alt'
                  size={14}
                  
                  color={colors.$grey}
                />
                 <Text style={styles.icontext}> 19-11-2021</Text>
            </View>
            <Divider style={styles.divider}/>
             <View style={[styles.iconlocation, {flexDirection:'column'}]}> 
            <Text style={styles.offertxt}>Feedback</Text>

            <TextInput style={styles.inputBox} placeholder='FeedBack' multiline={true}/>

            </View>
            </ScrollView>
        </View>
    )
}

export default ResortScreen

const styles = StyleSheet.create({
text:{
    fontSize: 18,
    fontWeight: '400',
    marginTop: 10,
    marginLeft: 20
},
image1:{
    height: hp(45),
    width: wp(90),
    alignSelf:'center',
    marginVertical: hp(1),
    borderRadius: 6
},
iconlocation:{
    marginLeft: 20,
    color: colors.$blackcolor,
    fontSize: 14,
    fontWeight:'400',
    marginTop: 5,
    flexDirection: 'row'
  },
  icontext:{
    color: colors.$grey,
    fontSize: 14,
    fontWeight:'400',
    marginTop: hp(-1/2),
    marginRight: wp(2),
    marginLeft: wp(1/2)
  },
  divider:{
      borderColor: '#767070',
      borderWidth: 1/2,
      marginLeft: wp(5),
      marginRight: wp(5),
      marginVertical: 10
  },
  offertxt:{
    color: colors.$blackcolor,
    fontSize: 14,
    fontWeight:'700',
    marginTop: hp(-1/2),
    marginRight: wp(2),
    marginLeft: wp(1/2)
  },
  inputBox:{
    height: hp(5),
    width: wp(90),
    borderColor: colors.$grey,
    borderWidth: 1/2,
    marginTop: 5,
    borderRadius: 10,
    marginBottom: 70,
    padding: 10
  }
})
