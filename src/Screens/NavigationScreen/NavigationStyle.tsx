import { StyleSheet } from "react-native";
import colors from "../../Styles/colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";


export const navigationStyle = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    imgtxt:{
      fontSize: RFPercentage(3.2),
      marginTop: hp(0.4),
      color: colors.$darkyellow,
      fontWeight: '700',
      textAlign:'center'
    }
});
