import React, { useState } from 'react';
import { Button, StyleSheet, Text, View, ScrollView, TouchableOpacity, Touchable, Modal, ImageBackground } from 'react-native';
import colors from '../../Styles/colors';
import TopIcon from '../../ReusableComponunts/TopIcon';
import * as Animatable from "react-native-animatable";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import ImageViewer from "react-native-image-zoom-viewer";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { GuestImage4, GuestImage5, GuestImage6, GuestImage7, CommonStyles  } from '../../Styles/CommonStyles';
import { homeStyle } from '../HomeScreen/HomeStyle';
import { navigationStyle } from './NavigationStyle';

const NavigationScreen = ({navigation}: any) => {
    const GotoAccount = () =>{}
    const [Show, setShow] = useState(false);
    const [Show1, setShow1] = useState(false);
    const [Show2, setShow2] = useState(false);
    const ConventionScreen = () => {
    setShow(!Show);
    setShow1(false);
    setShow2(false);
  };
  const ConventionScreen1 = () => {
    setShow(false);
    setShow1(!Show1);
    setShow2(false);
  };
  const ConventionScreen2 = () => {
    setShow(false);
    setShow1(false);
    setShow2(!Show2);
  };
  const [ModalVisibleStatus, setModalVisibleStatus] = useState(false);

  const ShowModalFunction = () => {
    setModalVisibleStatus(!ModalVisibleStatus);
  };
  const images = [
    {
      url: '',
      props: {
        source: require('../../../assets/img/guest.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest1.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest2.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest4.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest5.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest6.jpg') 
      }
    },
  ];
    return (
        <View style={{ backgroundColor: colors.$blackcolor, flex: 1 }}>
        <TopIcon/>
        <ScrollView>
           <TouchableOpacity onPress={ConventionScreen}>
               <Animatable.Image
               source={GuestImage4}
               animation='fadeInUp'
               style={CommonStyles.guestimage4}
               />
           </TouchableOpacity>
             {Show ? (
              <View>
                <FontAwesome
                  name='chain'
                  size={22}
                  color={colors.$darkyellow}
                  onPress={() => navigation.navigate("AmigoScreen")}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(-30), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                <Ionicons
                  onPress={() => ShowModalFunction()}
                  name='search'
                  size={22}
                  color={colors.$darkyellow}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(0), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                
              </View>
            ) : null}
            <Text style={navigationStyle.imgtxt}>Sea Island Resort</Text>
           <TouchableOpacity onPress={ConventionScreen1}>
               <Animatable.Image
               source={GuestImage5}
               animation='fadeInDown'
               style={CommonStyles.guestimage5}
               />
               
           </TouchableOpacity>
           {Show1 ? (
              <View>
                <FontAwesome
                  name='chain'
                  size={22}
                  color={colors.$darkyellow}
                  onPress={() => navigation.navigate("ResortScreen")}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(-33), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                <Ionicons
                  onPress={() => ShowModalFunction()}
                  name='search'
                  size={22}
                  color={colors.$darkyellow}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(0), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                
              </View>
            ) : null}
           <Text style={navigationStyle.imgtxt}>Sea Island Resort</Text>
           <TouchableOpacity onPress={ConventionScreen2}>
               <Animatable.Image
               source={GuestImage6}
               animation='fadeInUp'
               style={CommonStyles.guestimage6}
               />
               
           </TouchableOpacity>
           {Show2 ? (
              <View>
                <FontAwesome
                  name='chain'
                  size={22}
                  color={colors.$darkyellow}
                  onPress={() => navigation.navigate("ResortScreen")}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(-33), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                <Ionicons
                  onPress={() => ShowModalFunction()}
                  name='search'
                  size={22}
                  color={colors.$darkyellow}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(0), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                
              </View>
            ) : null}
            <Text style={navigationStyle.imgtxt}>Sea Island Resort</Text>
            <Text style={{marginBottom: 30}}></Text>
           <View style={homeStyle.imageModel}>
            <Modal
                    visible={ModalVisibleStatus}
                    transparent={false}
                    onRequestClose={() => ShowModalFunction()}>
                    <ImageViewer imageUrls={images} />
            </Modal>
            </View>
        </ScrollView>
        </View>
    )
}

export default NavigationScreen;