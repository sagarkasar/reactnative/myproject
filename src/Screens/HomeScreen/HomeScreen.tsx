import React, {useRef, useState} from "react";
import { View, Text, StyleSheet, Button, ScrollView, Image, TouchableOpacity, Modal, Linking } from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";
import colors from "../../Styles/colors";
import TopIcon from "../../ReusableComponunts/TopIcon";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import * as Animatable from "react-native-animatable";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { CommonStyles, GuestImage, GuestImage1, GuestImage2, GuestImage3 } from "../../Styles/CommonStyles";
import { homeStyle } from "./HomeStyle";


function HomeScreen({navigation}:any) {
  const [Show, setShow] = useState(false);
  const [Show1, setShow1] = useState(false);
  const [Show2, setShow2] = useState(false);
  const ConventionScreen = () => {
    setShow(!Show);
    setShow1(false);
    setShow2(false);
  };
  const ConventionScreen1 = () => {
    setShow(false);
    setShow1(!Show1);
    setShow2(false);
  };
  const ConventionScreen2 = () => {
    setShow(false);
    setShow1(false);
    setShow2(!Show2);
  };
  const url1 = "https://m.facebook.com/GuestGoldDotCom";
  const url2 = "https://mobile.twitter.com/GuestGoldDotCom";
  const url3 = "https://www.instagram.com/guestgolddotcom/";
  const openUrl = async (url: any) => {
    const isSuported = await Linking.canOpenURL(url);
    if (isSuported) {
      await Linking.openURL(url);
    } else {
      //  Alert.alert{`Not Found Url: $(url))`};
    }
  };
  const [ModalVisibleStatus, setModalVisibleStatus] = useState(false);

  const ShowModalFunction = () => {
    setModalVisibleStatus(!ModalVisibleStatus);
  };
  const images = [
    {
      url: '',
      props: {
        source: require('../../../assets/img/guest.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest1.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest2.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest4.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest5.jpg') 
      }
    },
    {
     url: '',
      props: {
        source: require('../../../assets/img/guest6.jpg') 
      }
    },
  ];
  return (
    <View style={{ backgroundColor: colors.$blackcolor, flex: 1 }}>
      <TopIcon/>
      <ScrollView>
        <TouchableOpacity onPress={ConventionScreen} /* onPress={()=> navigation.navigate('ResortScreen')} */>
        <Animatable.Image 
        source={GuestImage}
        animation='fadeInDownBig' 
        style={CommonStyles.guestimage}
        />
        
        {Show ? (
              <View>
                <FontAwesome
                
                  name='chain'
                  size={22}
                  color={colors.$darkyellow}
                  onPress={() => navigation.navigate("ConventionalScreen")}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(-22), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                <Ionicons
                  onPress={() => ShowModalFunction()}
                  name='search'
                  size={22}
                  color={colors.$darkyellow}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(0), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                
              </View>
            ) : null}
        </TouchableOpacity>
            <View style={{ flexDirection:'row',justifyContent:'space-between', marginTop: 5}}>
            <Text style={homeStyle.imgtext}>Sea Island Resort</Text>
            <Text style={homeStyle.imgrating}>
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$doubledarkgrey}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$doubledarkgrey}
              />
            </Text>
            </View>
            <Text style={homeStyle.iconlocation}> 
                <Ionicons
                  name='location-sharp'
                  size={12}
                  
                  color={colors.$darkyellow}
                />
                 <Text> Lorem ipsum dolor sit amet.</Text>
            </Text>
        <TouchableOpacity onPress={ConventionScreen1} /* onPress={()=> navigation.navigate('ResortScreen')} */>
        <Animatable.Image 
        source={GuestImage1} 
        style={CommonStyles.guestimage1} 
        animation='fadeInLeftBig'
        />
        {Show1 ? (
              <View>
                <FontAwesome
                  name='chain'
                  size={22}
                  color={colors.$darkyellow}
                  onPress={() => navigation.navigate("ResortScreen")}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(-22), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                <Ionicons
                  onPress={() => ShowModalFunction()}
                  name='search'
                  size={22}
                  color={colors.$darkyellow}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(0), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                
              </View>
            ) : null}
        </TouchableOpacity>
        <View style={{ flexDirection:'row',justifyContent:'space-between', marginTop: 5}}>
            <Text style={homeStyle.imgtext}>Sea Island Resort</Text>
            <Text style={homeStyle.imgrating}>
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$doubledarkgrey}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$doubledarkgrey}
              />
            </Text>
            </View>
            <Text style={homeStyle.iconlocation}> 
                <Ionicons
                  name='location-sharp'
                  size={12}
                  
                  color={colors.$darkyellow}
                />
                 <Text> Lorem ipsum dolor sit amet, consectetur adipiscing elit</Text>
            </Text>
        <TouchableOpacity onPress={ConventionScreen2} /* onPress={()=> navigation.navigate('ResortScreen')} */>
        <Animatable.Image 
        source={GuestImage2} 
        style={CommonStyles.guestimage2}
        animation='fadeInRightBig'
        />
        {Show2 ? (
              <View>
                <FontAwesome
                  name='chain'
                  size={22}
                  color={colors.$darkyellow}
                  onPress={() => navigation.navigate("ResortScreen")}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(-22), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                <Ionicons
                  onPress={() => ShowModalFunction()}
                  name='search'
                  size={22}
                  color={colors.$darkyellow}
                  style={[
                    homeStyle.hovericon,
                    { marginTop: hp(0), marginLeft: wp(35), padding: wp(4) },
                  ]}
                />
                
              </View>
            ) : null}
        </TouchableOpacity>
        <View style={{ flexDirection:'row',justifyContent:'space-between', marginTop: 5}}>
            <Text style={homeStyle.imgtext}>Sea Island Resort</Text>
            <Text style={homeStyle.imgrating}>
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$doubledarkgrey}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$doubledarkgrey}
              />
            </Text>
            </View>
            <Text style={homeStyle.iconlocation}> 
                <Ionicons
                  name='location-sharp'
                  size={12}
                  
                  color={colors.$darkyellow}
                />
                 <Text> Lorem ipsum dolor sit amet, consectetur adipiscing elit</Text>
            </Text>
        <TouchableOpacity onPress={()=> navigation.navigate('ResortScreen')}>
        <Image source={GuestImage3} style={CommonStyles.guestimage3}/>
        </TouchableOpacity>
        <View style={{ flexDirection:'row',justifyContent:'space-between', marginTop: 5}}>
            <Text style={homeStyle.imgtext}>Sea Island Resort</Text>
            <Text style={homeStyle.imgrating}>
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$darkyellow}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$doubledarkgrey}
              />
              <Entypo
              name='star'
              size= {13}
              color={colors.$doubledarkgrey}
              />
            </Text>
            </View>
            <Text style={homeStyle.iconlocation}> 
                <Ionicons
                  name='location-sharp'
                  size={12}
                  
                  color={colors.$darkyellow}
                />
                 <Text> Lorem ipsum dolor sit amet, consectetur adipiscing elit</Text>
            </Text>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={homeStyle.contacts}
                onPress={() => navigation.navigate("ContactUs")}>
                Contact
              </Text>
              <Text
                style={homeStyle.contacts}
                onPress={() => navigation.navigate("")}>
                About
              </Text>
              <EvilIcons
                onPress={() => {
                  openUrl(url1);
                }}
                name='sc-facebook'
                size={25}
                color='#d6af35'
                style={{ marginTop: hp(4.3), marginStart: wp(5) }}
              />

              <AntDesign
                onPress={() => {
                  openUrl(url2);
                }}
                name='twitter'
                size={25}
                color='#d6af35'
                style={{ marginTop: hp(4.3), marginStart: wp(5) }}
              />

              <AntDesign
                onPress={() => {
                  openUrl(url3);
                }}
                name='instagram'
                size={25}
                color='#d6af35'
                style={{
                  marginTop: hp(4.3),
                  marginStart: wp(6),
                }}
              />
            </View>
            <Text style={{marginBottom: 40}}></Text>
      </ScrollView>
      <View style={homeStyle.imageModel}>
          <Modal
                visible={ModalVisibleStatus}
                transparent={false}
                onRequestClose={() => ShowModalFunction()}>
                <ImageViewer imageUrls={images} />
          </Modal>
      </View>
    </View>
  );
}
export default HomeScreen;

