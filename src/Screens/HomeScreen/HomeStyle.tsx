import { StyleSheet } from "react-native";
import colors from "../../Styles/colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";


export const homeStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgtext:{
    fontSize: RFPercentage(3),
    fontWeight: '700',
    color:colors.$whitecolor,
    marginLeft: wp('6%'),
  },
  imgrating:{
    marginRight: wp(5),
    marginTop: hp(0.5)
  },
  iconlocation:{
    marginLeft: wp(5),
    color: colors.$whitecolor,
    fontSize: RFPercentage(2),
    fontWeight:'400',
    marginTop: hp(0.6)
  },
  hovericon: {
    top: wp(5),
    left: wp(8),
    width: wp(14),
    height: hp(7),
    borderRadius: wp(8),
    alignItems: "center",
    justifyContent: "center",
    shadowRadius: 10,
    shadowColor: "#fff",
    shadowOpacity: 0.3,
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    borderWidth: wp(1 / 2),
    borderColor: colors.$darkyellow,
  },
  imageModel:{
    flex: 1,
    alignItems: "center",
  },
  contacts: {
    color: colors.$darkyellow,
    fontSize: RFPercentage(3),
    textAlign: "center",
    marginStart: wp(10),
    marginTop: hp(4),
  },
});
