import React, { useState} from 'react';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import Fontisto from "react-native-vector-icons/Fontisto";
import TopIcon from '../../ReusableComponunts/TopIcon';
import { StyleSheet, View, Button, Text, Image, ScrollView, TextInput, Touchable, TouchableOpacity, ImageBackground, Linking, } from 'react-native';
import {  Divider } from 'react-native-elements';
import * as Animatable from "react-native-animatable";
import {   HotelImage, GuestRoom2, MexicanGrill, Property } from '../../Styles/CommonStyles';
import colors from '../../Styles/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { AmigoStyle } from './AmigoStyle';
const AmigoScreen =  ({navigation}: any) =>{

    const url1 = "https://www.elamigogrill.com/";
    const openUrl = async (url: any) => {
    const isSuported = await Linking.canOpenURL(url);
    if (isSuported) {
      await Linking.openURL(url);
    } 
    };

   return( 
   <View style={{ backgroundColor: colors.$blackcolor, flex: 1 }}>
       <TopIcon/>
    <ScrollView>
        <ImageBackground
            source={MexicanGrill}
            resizeMode='contain'
            style={AmigoStyle.guestimage4}
        />
        <Animatable.Text
            style={AmigoStyle.maintxt}
            animation='fadeInLeftBig'>
            El Amigo Mexican Grill
        </Animatable.Text>
        <Animatable.View
            style={{flexDirection:'row',justifyContent:'space-around', marginTop: hp(2)}}
            animation='fadeInRightBig'>
            <View style={{flexDirection:'row'}}>
            <FontAwesome
                name='photo'
                size={20}
                color={colors.$whitecolor}
              />
              <Text style={AmigoStyle.numberstyle}>1</Text>
              <Text
                style={AmigoStyle.numberstyle}>
                Photos
              </Text>
              </View>
              <View style={{flexDirection:'row'}}>
              <Ionicons
                name='ios-location-sharp'
                size={20}
                color={colors.$whitecolor}
              />

              <Text
                style={[
                  AmigoStyle.numberstyle,
                  {
                    color: colors.$darkyellow,
                  },
                ]}>
                333 State St,
                {"\n"}Erie, PA 16507, USA
              </Text>
              </View>
        </Animatable.View>
        <Divider width={2} color={colors.$darkyellow} style={AmigoStyle.divider}/>
        <View style={{ flexDirection: "row", justifyContent:'space-between',marginHorizontal: wp(30), marginTop: hp(1) }}>
                <Ionicons
                onPress={() => {
                  openUrl(url1);
                }}
                name='earth'
                size={30}
                color={colors.$whitecolor}
              />

              <AntDesign
                name='pluscircle'
                size={30}
                color={colors.$whitecolor}
              />

              <FontAwesome
                name='tag'
                size={30}
                color={colors.$whitecolor}
              />
        </View>
           
            <AntDesign
              name='tags'
              size={30}
              style={{ textAlign: "center", marginTop: hp(7) }}
              color={colors.$darkyellow}
            />
            <Divider width={2} color={colors.$darkyellow} style={[AmigoStyle.divider,{marginHorizontal: wp(42)}]}/>
            <Divider width={3} color={colors.$grey} />
            <TouchableOpacity>
                <Animatable.Image
                    animation='fadeInDown'
                    source={HotelImage}
                    style={AmigoStyle.hotel}
                    resizeMode='contain'
                />
            </TouchableOpacity>
            <Text style={AmigoStyle.imgtext}> Test 5</Text>
            <View style={{flexDirection:'row', alignSelf:'center'}}>
                <Entypo
                  name='trophy'
                  size={20}
                  style={{ paddingLeft: wp(1.5) }}
                  color={colors.$whitecolor}
                />
                <Text style={AmigoStyle.numberstyle1}>400.00 Points</Text>
                
                <Entypo
                  name='trophy'
                  size={20}
                  style={{ paddingLeft: wp(1.5) }}
                  color={colors.$whitecolor}
                />
                <Text style={AmigoStyle.numberstyle1}>Offer code : vgtxin</Text>

                <Fontisto
                  name='date'
                  size={15}
                  style={{ paddingLeft: wp(2) }}
                  color={colors.$whitecolor}
                />
                <Text style={[AmigoStyle.numberstyle1, { paddingRight: wp(2)}]}>
                  2021-08-12
                </Text>
            </View>
            <Divider width={3} color={colors.$grey} style={{marginTop:20}}/>
            <Divider width={3} color={colors.$grey} style={{marginTop:40}}/>

            <TouchableOpacity>
                <Animatable.Image
                    animation='fadeInDown'
                    source={Property}
                    style={AmigoStyle.hotel}
                    resizeMode='cover'
                />
            </TouchableOpacity>

             <Divider width={3} color={colors.$grey} style={{marginTop:10}}/>
              <Ionicons
                name='home'
                size={30}
                style={{ textAlign: "center", marginTop: hp(2) }}
                color={colors.$darkyellow}
              />
             <Divider width={2} color={colors.$darkyellow} style={[AmigoStyle.divider,{marginHorizontal: wp(42)}]}/>
              <Divider width={3} color={colors.$grey} />
            
            <Divider width={3} color={colors.$grey} />
            <TouchableOpacity>
                <Animatable.Image
                    animation='fadeInDown'
                    source={HotelImage}
                    style={AmigoStyle.hotel}
                    resizeMode='contain'
                />
            </TouchableOpacity>
            <Divider width={3} color={colors.$grey} />
              <View style={{ marginBottom: 70}}></View>
    </ScrollView>
    </View>
   )
}

export default AmigoScreen;