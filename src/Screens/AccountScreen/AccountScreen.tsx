import React, { Component, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput  } from 'react-native';
import { accountStyle } from './AccountStyle';
import colors from '../../Styles/colors';
import * as yup from "yup";
import { Formik } from "formik";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { CommonStyles, TopImage } from '../../Styles/CommonStyles';
import { Checkbox } from "react-native-paper";
import axios from 'axios';
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { RFPercentage } from 'react-native-responsive-fontsize';
//import DropDown from '../../ReusableComponunts/DropDown';

const data = require("../../ReusableComponunts/data.json");

const AccountScreen = ({navigation}: any) => {
    const GotoAccount = () =>{}
    const [secure, setSecure] = React.useState(true);
    const [checked, setChecked] = useState(true);
    // const [checked1, setChecked1] = useState(true);
    const [UserName, setUserName] = useState('');
    const [Password, setPassword] = useState('');
    
    const onSubmit = async (values: any) => {
      navigation.navigate('TabRoutes')
      console.log(values);
    
  };

  
   /*  const handleSubmit = () =>{
      navigation.navigate(TabRoutes)

    } */
    return (
        <View style={{ backgroundColor: colors.$blackcolor, flex: 1 }}>
        
          <View style={accountStyle.container}> 
            <View style={accountStyle.Viewimg}>
              <Image 
                source={TopImage}
                style={accountStyle.logoimg}
              />
            </View> 
            <Text style={{color:colors.$whitecolor, fontSize: RFPercentage(4), fontWeight: '700'}}>LOGIN</Text>
            <Formik 
            initialValues={{
              UserName: '',
              Password: '',
            }}
            onSubmit={onSubmit}
            validationSchema={
              yup.object().shape({
                UserName: yup.string().required(),
                Password: yup.string().required(),
              })}>
                {({
                  values,
                  handleChange,
                  errors,
                  setFieldTouched,
                  touched,
                  isValid,
                  handleSubmit,
              }) => (
                <View>
            <TextInput 
            style={accountStyle.inputtext} 
            placeholderTextColor={colors.$whitecolor} 
            placeholder="UserName"
            defaultValue={values.UserName}
            onChangeText={handleChange('UserName')}
            onChange={() => setUserName(UserName)}
            onBlur={()=> setFieldTouched('UserName')}
            />
             {touched.UserName && errors.UserName && (
                <Text style={[CommonStyles.error,{marginTop:wp(-1) }]}>{errors.UserName}</Text>
              )}
            
            <TextInput 
            secureTextEntry={secure}
            style={accountStyle.inputtext} 
            placeholderTextColor='white' 
            placeholder="Password"
            textContentType= 'password'
            defaultValue={values.Password}
            onChangeText={handleChange('Password')}
            onChange={() => setPassword(Password)}
            onBlur={()=> setFieldTouched('Password')}
            />
            
            <Ionicons 
              name={secure ? "eye-off" : 'eye'}
              size={RFPercentage(3.4)}
              color={colors.$darkyellow}
              style={accountStyle.showicon}
              onPress={() => setSecure(!secure)}
            /> 
             {touched.Password && errors.Password && (
                <Text style={[CommonStyles.error, {marginTop:wp(-3) }]}>{errors.Password}</Text>
              )}
            <TouchableOpacity
            style={{alignSelf:'flex-end', marginRight: wp(4)}}
            >
              <Text style={[accountStyle.checkboxtext, {marginTop:wp(-2) }]}>Forgot Password?</Text>
            </TouchableOpacity>
             <View style={accountStyle.checkboxview}> 
             <Checkbox
                  status={checked ? "checked" : "unchecked"}
                  onPress={() => {
                    setChecked(!checked);
                  }}
                  color={colors.$blue}
                  uncheckedColor={colors.$whitecolor}
                />
                <Text style={[accountStyle.checkboxtext,{marginBottom: wp(6)}]}>
                  Accept Terms & Condition 
                </Text>
            </View>
            {/* <DropDown 
              defaultValue={typestatus || ""}
              items={data.promotiondropdwon}
              placeholder='Please select type'
              onSelect={(item: any) => Typefunction(item)}
              
            /> */}
            <TouchableOpacity
            onPress={() => handleSubmit()}
            disabled={!isValid}
            >
              <Text style={accountStyle.loginbtn}>Login</Text>
            </TouchableOpacity>
            </View>
          )}
            </Formik>
            <View style={{flexDirection:'row'}}>
            <Text style={accountStyle.text}>don't have an account</Text>
            <TouchableOpacity>
              <Text style={accountStyle.signupbtn} onPress={()=> navigation.navigate('SignUpScreen')}> SIGN UP</Text>
            </TouchableOpacity>
            </View>
          </View>
        </View>
    )
}

export default AccountScreen;
/* const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily:'Century Gothic',   
  },
  Viewimg:{
    height: 110,
    width: 110,
    borderRadius: 60,
    borderWidth: wp(1 / 2),
    borderColor: colors.$darkyellow,
    marginBottom: 20
  },
  logoimg:{
    alignSelf:'center',
    marginTop:20,
    height: 65,
    width: 53,
  },
  loginbtn:{
    backgroundColor: colors.$darkyellow,
    color: colors.$whitecolor,
    padding: 10,
    borderRadius:70,
    height: 46,
    width: 292,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: "700"
  },
  signupbtn:{
    color: colors.$darkyellow,
    fontSize: 14,
    margin: 10,
    fontWeight:'400'
  },
  text:{
    color: colors.$whitecolor,
    fontSize: 14,
     marginTop: 20
  },
  checkboxtext:{
    color: colors.$whitecolor,
    fontSize: 12,
    marginTop: 7,
    fontWeight:'400'
  },
  checkboxview:{
    alignSelf:'flex-start',
    marginLeft: 15,
    flexDirection:'row'
  },
  inputtext:{
    borderRadius: 70,
    //backgroundColor:colors.$whitecolor,
    color: colors.$whitecolor,
    height: 46,
    width: 292,
    margin: 12,
    borderWidth: 1,
    borderColor:colors.$whitecolor,
    padding: 10,
    paddingLeft: 20
  },
  dropdown:{

  }
  
}); */