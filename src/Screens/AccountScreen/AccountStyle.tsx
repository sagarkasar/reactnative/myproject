import { StyleSheet } from "react-native";
import colors from "../../Styles/colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";


export const accountStyle = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily:'Century Gothic',   
  },
  Viewimg:{
    height: hp('16%'),
    width: wp('32%'),
    borderRadius: 100,
    borderWidth: wp(1 / 2),
    borderColor: colors.$darkyellow,
    marginBottom: hp(4)
  },
  logoimg:{
    alignSelf:'center',
    marginTop:hp(3),
    height: hp('9%'),
    width: wp('9%'),
  },
  loginbtn:{
    backgroundColor: colors.$darkyellow,
    color: colors.$whitecolor,
    padding: '3%',
    borderRadius:wp(10),
    height: hp('6.5%'),
    width: wp('80%'),
    fontSize: RFPercentage(2.9),
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: "700"
  },
  signupbtn:{
    color: colors.$darkyellow,
    fontSize: RFPercentage(2.3),
    margin: '3%',
    fontWeight:'400',
    marginTop: wp(6)
  },
  text:{
    color: colors.$whitecolor,
    fontSize: RFPercentage(2.2),
    marginTop: wp(6)
  },
  checkboxtext:{
    color: colors.$whitecolor,
    fontSize: RFPercentage(2.2),
    marginTop: hp(1),
    fontWeight:'400'
  },
  checkboxview:{
    alignSelf:'flex-start',
    marginLeft: wp(4),
    flexDirection:'row'
  },
  inputtext:{
    borderRadius: wp(10),
    //backgroundColor:colors.$whitecolor,
    color: colors.$whitecolor,
    height: hp('6%'),
    width: wp('80%'),
    margin: '3%',
    borderWidth: 1,
    borderColor:colors.$whitecolor,
    padding: '2%',
    paddingLeft: wp(4)
  },
  showicon:{
    alignSelf:'flex-end',
    marginRight: wp(3.5),
    marginTop: hp(-7.4),
    marginBottom: wp(5),
    padding: '2.1%',
    backgroundColor:colors.$darkgrey,
    borderRadius: wp(10)
  }
})

