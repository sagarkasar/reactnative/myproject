export default {
  $darkyellow: "#d6af35",
  $blackcolor: "#000",
  $whitecolor: "#fff",
  $greycolor: "#36414d",
  $red: "#FF0000",
  $grey: "#666",
  $blue: "#4085d6",
  $darkgrey: "#231f20",
  $doubledarkgrey: "#4a4a4a",
};
