import { StyleSheet } from "react-native";
import colors from "./colors";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const TopImage = require("../../assets/img/logo-newLeft.png");
const GuestImage = require("../../assets/img/guest.jpg");
const GuestImage1 = require("../../assets/img/guest1.jpg");
const GuestImage2 = require("../../assets/img/guest2.jpg");
const GuestImage3 = require("../../assets/img/guest3.jpg");
const BackgroundLogoImage = require("../../assets/img/Guest_bgNew.png");
const GuestImage4 = require("../../assets/img/guest4.jpg");
const GuestImage5 = require("../../assets/img/guest5.jpg");
const GuestImage6 = require("../../assets/img/guest6.jpg");
const GuestImage7 = require("../../assets/img/guest7.jpg");
const MexicanGrill = require("../../assets/img/mexican_grill.jpg");
const HotelImage = require("../../assets/img/hotel.png");
const Property = require("../../assets/img/property.jpg");
const Brid = require("../../assets/img/brid.png");
const GuestRoom = require("../../assets/img/guestroom.jpg");
const GuestRoom1 = require("../../assets/img/guestroom2.jpg");
const GuestRoom2 = require("../../assets/img/guestroom3.jpg");

const CommonStyles = StyleSheet.create({
  topimage: {
    height: hp(6.5),
    marginTop: hp(6.5),
    width: wp(20),
    backgroundColor: "transparent",
    alignSelf: "flex-start",
    marginBottom: -10,
    position: "relative",
    zIndex: 9,
  },
  dropdownPicker: {
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderWidth: 1,
    borderColor: colors.$grey,
    backgroundColor: colors.$whitecolor,
    color: "#000",
    width: wp(89),
    height: hp(6),
    marginStart: wp(6),
    fontSize: 14,
    marginBottom: 10,
    padding: hp(0.7),
  },
  guestimage: {
    height: hp(25),
    width: wp(95),
    marginTop: hp(2),
    marginHorizontal: wp(2),
    backgroundColor: "transparent",
    alignItems: "flex-start",
    borderRadius: wp(1)
  },

  guestimage1: {
    height: hp(25),
    marginTop: hp(4.5),
    marginHorizontal: wp(2),
    width: wp(95),
    backgroundColor: "transparent",
    alignItems: "flex-start",
    borderRadius: wp(1),
  },

  guestimage2: {
    height: hp(25),
    marginTop: hp(4.5),
    marginHorizontal: wp(2),
    width: wp(95),
    backgroundColor: "transparent",
    alignItems: "flex-start",
    borderRadius: wp(1)
  },

  guestimage3: {
    height: hp(25),
    marginTop: hp(4.5),
    marginHorizontal: wp(2),
    width: wp(95),
    backgroundColor: "transparent",
    alignItems: "flex-start",
    borderRadius: wp(1)
  },
  guestimage4: {
    height: hp(35),
    marginTop: hp(4.5),
    width: wp(100),
    backgroundColor: "transparent",
    alignItems: "flex-start",
  },
  guestimage5: {
    height: hp(50),
    marginTop: hp(4.5),
    width: wp(100),
    backgroundColor: "transparent",
    alignItems: "flex-start",
  },
  guestimage6: {
    height: hp(50),
    marginTop: hp(4.5),
    width: wp(100),
    backgroundColor: "transparent",
    alignItems: "flex-start",
  },
  guestimage7: {
    height: hp(60),
    marginTop: hp(4.5),
    width: wp(100),
    backgroundColor: "transparent",
    alignItems: "flex-start",
  },
  AboutBackimg: {
    height: hp(15),
    marginTop: hp(3),
    //width: wp(70),
    backgroundColor: "transparent",
    alignItems: "center",
    //marginRight: wp(14),
  },
  BackgroundLogoImg: {
    height: hp(25),
    marginTop: hp(1),
    width: wp(85),
    backgroundColor: "transparent",
    alignItems: "flex-end",
  },
  aboutimg: {
    height: hp(70),
    marginTop: hp(4.5),
    width: wp(90),
    backgroundColor: "transparent",
    alignItems: "flex-start",
    margin: 20,
  },
  BackgroundLogoImage: {
    height: hp(25),
    marginTop: hp(0.5),
    width: wp(58),
    backgroundColor: "transparent",
    alignSelf: "center",
  },
  textstyle: {
    fontSize: RFPercentage(3),
    color: colors.$whitecolor,
    textAlign: "center",
    marginTop: hp(2),
    fontFamily: "Poppins_700Bold",
  },
  Modalviewstyle: {
    width: wp(93.5),
    height: "auto",
    marginStart: wp(-1.5),
    backgroundColor: colors.$darkyellow,
    borderWidth: 0.5,
    borderColor: colors.$whitecolor,
    borderRadius: 15,
    borderBottomLeftRadius: 20,
  },
  BackgroundLogoImageGuest: {
    height: hp(25),
    marginTop: hp(0.5),
    width: wp(50),
    backgroundColor: "transparent",
    alignSelf: "center",
  },
  inputbox: {
    borderStyle: "solid",
    paddingLeft: 20,
    paddingRight: 20,
    fontFamily: "Poppins_400Regular",
    borderWidth: 1,
    borderColor: "#cccccc",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    height: hp(6),
    width: wp(80),
    marginTop: hp(6),
    backgroundColor: colors.$whitecolor,
    overflow: "hidden",
    color: colors.$darkyellow,
    fontSize: 14,
    marginStart: wp(6.5),
  },
  error: {
    fontSize: 14,
    color: colors.$red,
    marginLeft: wp(8),
   
    //fontFamily: "Poppins_400Regular",
  },
  iconstyle: {
    backgroundColor: colors.$blackcolor,
    borderWidth: 2,
    borderColor: colors.$whitecolor,
    height: 100,
    width: 100,
    borderRadius: 100,
    left: "50%",
    top: -50,
    margin: "auto",
    position: "absolute",
    zIndex: 999,
    marginLeft: -50,
  },
  GuestImagescreen: {
    height: hp(104.5),
    width: wp(100),
    backgroundColor: "transparent",
    alignItems: "flex-start",
  },
});
export {
  CommonStyles,
  TopImage,
  GuestImage,
  GuestImage1,
  GuestImage2,
  GuestImage3,
  GuestImage4,
  GuestImage5,
  GuestImage6,
  GuestImage7,
  BackgroundLogoImage,
  MexicanGrill,
  HotelImage,
  Property,
  Brid,
  GuestRoom,
  GuestRoom1,
  GuestRoom2,
};
